====================
django-phone-checker
====================

It is simple web-service for checking a phone numbers.

Installation
------------

.. code-block:: shell

    pip3 install django-phone-checker
    # or
    pip3 install git+https://gitlab.com/djbaldey/django-phone-checker.git@master#egg=django-phone-checker


Quick start
-----------

1. Add "phone_checker" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'phone_checker',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('phone-checker/', include('phone_checker.urls')),

3. Run `python3 manage.py migrate` to create the necessary models.

4. Run `python3 manage.py upgrade_russian_phones` to fill database.

5. Start the development server and visit http://127.0.0.1:8000/phone-checker/
   to check your phone numbers.
