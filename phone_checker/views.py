#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.decorators.cache import cache_page

from phone_checker.forms import PhoneForm


@cache_page(60 * 15)
def index(request):
    """Display interactive form."""

    ctx = {}
    if 'phone' in request.GET:
        form = PhoneForm(request.GET)
        if form.is_valid():
            ctx['phone'] = form.cleaned_data['phone']
            ctx['info'] = form.info
    else:
        form = PhoneForm()
    ctx['form'] = form
    return render(request, 'phone_checker/index.html', ctx)


@cache_page(60 * 15)
def check(request):
    """Check the phone number and returns result as JSON."""

    form = PhoneForm(request.GET)
    if form.is_valid():
        data = {
            'phone': form.cleaned_data['phone'],
            'info': form.info,
        }
    else:
        data = {
            'errors': form.errors.get_json_data(),
        }
    params = {'indent': 2, 'ensure_ascii': False}
    return JsonResponse(data, json_dumps_params=params)
