#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.db import models
from django.utils.translation import gettext_lazy as _


class Band(models.Model):
    """Model for band of phone numbers."""
    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated = models.DateTimeField(_('updated'), auto_now=True)
    # Первый номер диапазона.
    first_number = models.BigIntegerField(_('first number'), db_index=True)
    # Последний номер диапазона.
    last_number = models.BigIntegerField(_('last number'), db_index=True)
    # Индексированное кол-во номеров необходимо для сортировки.
    volume = models.IntegerField(_('volume of numbers'), db_index=True)
    # Поле с информацией о месте привязки номеров.
    info = models.TextField(_('information'))
    # Скрытое поле для отличия устаревших записей во время синхронизации с
    # внешними источниками.
    for_upgrade = models.BooleanField(
        default=False, editable=False, db_index=True)

    class Meta:
        verbose_name = _('number band')
        verbose_name_plural = _('number bands')
        unique_together = ('first_number', 'last_number')
        ordering = ('first_number', 'volume')
