��          �      ,      �     �     �     �     �     �     �  u         �     �     �     �     �     �     �     �     �  �       �  .   �  D     .   G  9   v     �  �   �  N   �     �     �          )     G     e     �     �                             
                      	                           Check Checking the phone number Enter a valid phone number. Enter the phone number Phone number checking form The number is not found. The phone number begins with the " + " sign and contains only digits. No spaces, hyphens, brackets and other symbols. The phone number is not integer. created first number information last number number band number bands updated volume of numbers Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-11-16 20:25+1000
Last-Translator: Grigoriy Kramarenko <root@rosix.ru>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Проверить Проверка номера телефона Введите правильный телефонный номер. Введите телефонный номер Форма проверки номера телефона Номер не найден. Телефонный номер начинается со знака " + " и содержит только цифры. Без пробелов, дефисов, скобок и прочих символов. Телефонный номер не является целым числом. создана первый номер информация последний номер номерная полоса номерные полосы обновлена объём номеров 