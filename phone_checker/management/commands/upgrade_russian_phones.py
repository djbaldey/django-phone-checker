import csv
import ctypes
import os
import shutil
from urllib.request import urlopen

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.db.transaction import atomic
from django.utils.crypto import get_random_string
from django.utils.timezone import now

from phone_checker.models import Band


REMOTE_FILES = [
    'https://rossvyaz.ru/data/ABC-3xx.csv',
    'https://rossvyaz.ru/data/ABC-4xx.csv',
    'https://rossvyaz.ru/data/ABC-8xx.csv',
    'https://rossvyaz.ru/data/DEF-9xx.csv',
]


def parse_row(row):
    first_number = int('7' + row[0] + row[1])
    last_number = int('7' + row[0] + row[2])
    volume = int(row[3])
    assert last_number - first_number == volume - 1
    data = {
        'first_number': first_number,
        'last_number': last_number,
        'volume': volume,
        'info': '\n'.join(['Russia'] + row[4:]),
    }
    return data


class Command(BaseCommand):

    help = 'Upgrade the russian number bands of phones.'

    def handle(self, **options):
        verb = options['verbosity']
        stdout = self.stdout.write
        stderr = self.stderr.write

        def log(level, message, *args, **kwargs):
            if verb >= level:
                if kwargs:
                    message %= kwargs
                elif args:
                    message %= args
                stdout(message)
        begin = now()
        log(1, 'BEGIN: %s', begin)

        # Сначала скачаем данные, чтобы не обгадиться, когда сеть недоступна.
        tmpdir = '/tmp/rossvyaz_files_%s' % get_random_string(8)
        os.makedirs(tmpdir)
        log(2, 'CREATE: %s', tmpdir)
        files = []
        try:
            for url in REMOTE_FILES:
                log(2, 'GET: %s', url)
                filename = os.path.join(tmpdir, os.path.basename(url))
                with urlopen(url) as response:
                    with open(filename, 'wb') as f:
                        f.write(response.read())
                        files.append(filename)
                        log(2, 'SAVE: %s', filename)
        except Exception as e:
            stderr(str(e))
            files = []

        if not files:
            stderr('The downloading of files is failed.')
            shutil.rmtree(tmpdir)
            return

        ru_qs = Band.objects.filter(
            Q(first_number__gte=73000000000, last_number__lte=73999999999) |
            Q(first_number__gte=74000000000, last_number__lte=74999999999) |
            Q(first_number__gte=78000000000, last_number__lte=79999999999)
        )
        ru_qs.update(for_upgrade=True)

        csv.field_size_limit(int(ctypes.c_ulong(-1).value // 2))
        created = updated = skipped = 0
        bulk_list = []
        save_list = []
        skip_list = []
        update_fields = ('updated', 'info', 'for_upgrade')
        for filename in files:
            log(2, 'PARSE: %s', filename)
            with open(filename, newline='', encoding='utf-8') as csvfile:
                reader = csv.reader(csvfile, delimiter=';')
                for i, row in enumerate(reader):
                    # Удаление символа обозначения кодировки.
                    if i == 0 and row[0].startswith('\ufeff'):
                        row[0] = row[0].replace('\ufeff', '')
                        if verb > 1:
                            stdout('Cut out the symbol \\ufeff')
                    try:
                        data = parse_row(row)
                    except Exception as e:
                        stderr('Error in %d row. %s' % (i, str(e)))
                        break
                    band = ru_qs.filter(
                        first_number=data['first_number'],
                        last_number=data['last_number'],
                    ).first()
                    if not band:
                        band = Band(**data)
                        bulk_list.append(band)
                        created += 1
                    elif band.info != data['info']:
                        band.info = data['info']
                        band.for_upgrade = False
                        save_list.append(band)
                        updated += 1
                    else:
                        skip_list.append(band.id)
                        skipped += 1
                    # Создаём по 100 штук.
                    length = len(bulk_list)
                    if length >= 100:
                        Band.objects.bulk_create(bulk_list)
                        log(2, 'BULK: %s', length)
                        bulk_list.clear()
                    # Сохраняем по 100 штук.
                    length = len(save_list)
                    if length >= 100:
                        with atomic():
                            for band in save_list:
                                band.save(update_fields=update_fields)
                            log(2, 'SAVE: %s', length)
                            save_list.clear()
                    # Обновляем 1000 неизменёных дату и сбрасываем флаг.
                    length = len(skip_list)
                    if length >= 1000:
                        Band.objects.filter(id__in=skip_list).update(
                            updated=now(), for_upgrade=False)
                        log(2, 'SKIP: %s', length)
                        skip_list.clear()
        # Создаём оставшиеся.
        if bulk_list:
            Band.objects.bulk_create(bulk_list)
            log(2, 'BULK: %s', len(bulk_list))
            bulk_list.clear()
        # Сохраняем оставшиеся
        if save_list:
            with atomic():
                for band in save_list:
                    band.save(update_fields=update_fields)
                log(2, 'SAVE: %s', len(save_list))
                save_list.clear()
        # Обновляем неизменённые.
        if skip_list:
            Band.objects.filter(id__in=skip_list).update(
                updated=now(), for_upgrade=False)
            log(2, 'SKIP: %s', len(skip_list))
            skip_list.clear()

        shutil.rmtree(tmpdir)
        log(2, 'REMOVE: %s', tmpdir)
        # Удаляем оставшиеся с не сброшенным флагом обновления.
        deleted = ru_qs.filter(for_upgrade=True).delete()[0]
        log(1, 'Created %d, updated %d, skipped %d, deleted %d phone bands.',
            created, updated, skipped, deleted)
        end = now()
        log(1, 'END: %s, %d seconds', end, (end - begin).total_seconds())
