#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django import forms
from django.utils.translation import gettext_lazy as _

from phone_checker.models import Band
from phone_checker.validators import PhoneValidator


class PhoneForm(forms.Form):

    phone = forms.CharField(
        required=True,
        validators=[PhoneValidator()],
        label=_('Enter the phone number'),
        help_text=_(
            'The phone number begins with the " + " sign and contains '
            'only digits. No spaces, hyphens, brackets and other symbols.'
        ),
    )

    def clean_phone(self):
        value = self.cleaned_data['phone']
        try:
            phone = int(value)
        except ValueError:
            raise forms.ValidationError(_('The phone number is not integer.'))
        qs = Band.objects.filter(
            first_number__lte=phone, last_number__gte=phone,
        )
        band = qs.order_by('volume').first()
        if band:
            self.info = band.info
        else:
            raise forms.ValidationError(_('The number is not found.'))
        return '+%s' % phone
