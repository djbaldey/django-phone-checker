#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.urls import path
from phone_checker.views import index, check

app_name = 'phone_checker'


urlpatterns = [
    path('', index, name='index'),
    path('check', check, name='check'),
]
