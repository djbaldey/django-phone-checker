#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import sys
from django.db.utils import DatabaseError
from django.test import TestCase

from phone_checker.models import Band


class BandTestCase(TestCase):

    def test_create_zero(self):
        band = Band(first_number=0, last_number=0, volume=0, info='Zero')
        band.save()
        with self.assertRaises(DatabaseError):
            band.pk = None
            band.save()

    def test_create_max(self):
        val = sys.maxsize
        band = Band(first_number=val, last_number=val, volume=0, info='Max')
        band.save()
        with self.assertRaises(DatabaseError):
            band.pk = None
            band.save()
