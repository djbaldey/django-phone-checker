#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.test import TestCase
from django.urls import reverse

from phone_checker.models import Band


class ViewsTestCase(TestCase):

    def setUp(self):
        Band.objects.bulk_create([
            Band(first_number=78000000000, last_number=78000000009, volume=10,
                 info='[78] 10 numbers'),
            Band(first_number=78000000000, last_number=78000000099, volume=100,
                 info='[78] 100 numbers'),
            Band(first_number=79000000000, last_number=79000000099, volume=100,
                 info='[79] 100 numbers'),
        ])

    def test_index(self):
        url = reverse('phone_checker:index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertNotIn('phone', response.context)
        self.assertNotIn('info', response.context)

    def test_index_78000000000(self):
        url = reverse('phone_checker:index')
        for n in range(10):
            phone = 78000000000 + n
            response = self.client.get(url, {'phone': str(phone)})
            ctx = response.context
            self.assertIn('phone', ctx)
            self.assertIn('info', ctx)
            self.assertEqual(ctx['phone'], '+%d' % phone)
            self.assertEqual(ctx['info'], '[78] 10 numbers')

    def test_index_78000000010(self):
        url = reverse('phone_checker:index')
        for n in range(10):
            phone = 78000000010 + n
            response = self.client.get(url, {'phone': str(phone)})
            ctx = response.context
            self.assertIn('phone', ctx)
            self.assertIn('info', ctx)
            self.assertEqual(ctx['phone'], '+%d' % phone)
            self.assertEqual(ctx['info'], '[78] 100 numbers')

    def test_index_79000000000(self):
        url = reverse('phone_checker:index')
        for n in range(100):
            phone = 79000000000 + n
            response = self.client.get(url, {'phone': str(phone)})
            ctx = response.context
            self.assertIn('phone', ctx)
            self.assertIn('info', ctx)
            self.assertEqual(ctx['phone'], '+%d' % phone)
            self.assertEqual(ctx['info'], '[79] 100 numbers')

    def test_check_78000000000(self):
        url = reverse('phone_checker:check')
        for n in range(10):
            phone = 78000000000 + n
            response = self.client.get(url, {'phone': str(phone)})
            self.assertEqual(response.status_code, 200, response.content)
            data = response.json()
            self.assertIn('phone', data)
            self.assertIn('info', data)
            self.assertEqual(data['phone'], '+%d' % phone)
            error = 'Broken sorting in SQL'
            self.assertEqual(data['info'], '[78] 10 numbers', error)
